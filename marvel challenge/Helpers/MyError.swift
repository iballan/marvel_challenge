//
//  MyError.swift
//  marvel challenge
//
//  Created by MBH on 11/25/21.
//

import Foundation

public class MyError: LocalizedError {
    var message: String!
    init(message: String) {
        self.message = message
    }

    public var errorDescription: String? { message }
    public var localizedDescription: String? { message }
}
