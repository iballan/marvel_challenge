//
//  MarvelWebService.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation
import Moya

enum MarvelWebService {
    case getChars(limit: Int, offset: Int)
    case getCharDetails(id: Int)
    case getComics(id:  Int)
}

extension MarvelWebService: TargetType {
    
    var baseURL: URL {
        return URL(string: Constants.BaseUrl)!
    }

    var path: String {
        switch self {
        case .getChars:
            return "/characters"
        case .getCharDetails(let id):
            return "/characters/\(id)"
        case .getComics(let id):
            return "/characters/\(id)/comics"
        }
    }

    var method: Moya.Method {
        return .get
    }

    var task: Moya.Task {
        switch self {
        case .getChars(let limit, let offset):
            return .requestParameters(parameters: addSecurityParams(params:[
                "limit":limit, "offset":offset,
                "orderBy":"name"
            ]), encoding: URLEncoding.queryString)
        case .getCharDetails:
            return .requestParameters(parameters: addSecurityParams(), encoding: URLEncoding.queryString)
        case .getComics:
            return .requestParameters(parameters: addSecurityParams(params:[
                "startYear":2005,
                "orderBy":"-focDate",
                "limit" : 10
            ]), encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
    
    var sampleData: Data {
        return Data()
    }
    
    private func addSecurityParams(params: [String: Any] = [:]) -> [String: Any]{
        var dict = [String: Any]()
        params.forEach { key, value in
            dict[key] = value
        }
        dict["ts"] = "123123"
        dict["apikey"] = Constants.publicKey
        dict["hash"] = "\(dict["ts"]!)\(Constants.privateKey)\(Constants.publicKey)".md5()
        return dict
    }

}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    var utf8Encoded: Data { Data(self.utf8) }
}
