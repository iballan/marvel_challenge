//
//  ComicsListResult.swift
//  marvel challenge
//
//  Created by MBH on 11/25/21.
//

import Foundation
import ObjectMapper

struct ComicsListResult : Mappable {
    var code : Int?
    var status : String?
    var message: String?
    var copyright : String?
    var attributionText : String?
    var attributionHTML : String?
    var etag : String?
    var data : ComicsData?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        code <- map["code"]
        status <- map["status"]
        message <- map["message"]
        copyright <- map["copyright"]
        attributionText <- map["attributionText"]
        attributionHTML <- map["attributionHTML"]
        etag <- map["etag"]
        data <- map["data"]
    }

}
struct ComicsData : Mappable {
    var offset : Int?
    var limit : Int?
    var total : Int?
    var count : Int?
    var results : [Comics]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        offset <- map["offset"]
        limit <- map["limit"]
        total <- map["total"]
        count <- map["count"]
        results <- map["results"]
    }

}
