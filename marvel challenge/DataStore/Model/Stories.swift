
import Foundation
import ObjectMapper

struct Stories : Mappable {
	var available : Int?
	var collectionURI : String?
	var items : [Items]?
	var returned : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		available <- map["available"]
		collectionURI <- map["collectionURI"]
		items <- map["items"]
		returned <- map["returned"]
	}

}
