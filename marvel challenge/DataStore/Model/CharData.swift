
import Foundation
import ObjectMapper

struct CharData : Mappable {
	var offset : Int?
	var limit : Int?
	var total : Int?
	var count : Int?
	var results : [MCharacter]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		offset <- map["offset"]
		limit <- map["limit"]
		total <- map["total"]
		count <- map["count"]
		results <- map["results"]
	}

}
