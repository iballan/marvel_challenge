
import Foundation
import ObjectMapper

struct Comics : Mappable {
	var available : Int?
	var collectionURI : String?
	var items : [Items]?
	var returned : Int?
    var title: String?
    var dates: [ComicsDate]?
    var thumbnail: Thumbnail?
    var images: [Thumbnail]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		available <- map["available"]
		collectionURI <- map["collectionURI"]
		items <- map["items"]
		returned <- map["returned"]
        title <- map["title"]
        thumbnail <- map["thumbnail"]
        images <- map["images"]
	}

}

struct ComicsDate : Mappable {
    var type: String?
    var date: Date?
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        type <- map["type"]
        date <- (map["date"], DateTransform())
    }

}
