
import Foundation
import ObjectMapper

struct Items : Mappable {
	var resourceURI : String?
	var name : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		resourceURI <- map["resourceURI"]
		name <- map["name"]
	}

}
