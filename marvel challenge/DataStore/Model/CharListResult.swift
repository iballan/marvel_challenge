

import Foundation
import ObjectMapper

struct CharListResult : Mappable {
	var code : Int?
	var status : String?
    var message: String?
	var copyright : String?
	var attributionText : String?
	var attributionHTML : String?
	var etag : String?
	var data : CharData?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		code <- map["code"]
		status <- map["status"]
        message <- map["message"]
		copyright <- map["copyright"]
		attributionText <- map["attributionText"]
		attributionHTML <- map["attributionHTML"]
		etag <- map["etag"]
		data <- map["data"]
	}

}
