
import Foundation
import ObjectMapper

struct Thumbnail : Mappable {
	var path : String?
	var ext : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		path <- map["path"]
		ext <- map["extension"]
	}

}
