
import Foundation
import ObjectMapper

struct MCharacter : Mappable {
	var id : Int?
	var name : String?
	var description : String?
	var modified : String?
	var thumbnail : Thumbnail?
	var resourceURI : String?
	var comics : Comics?
	var series : Series?
	var stories : Stories?
	var events : Events?
	var urls : [Urls]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
		description <- map["description"]
		modified <- map["modified"]
		thumbnail <- map["thumbnail"]
		resourceURI <- map["resourceURI"]
		comics <- map["comics"]
		series <- map["series"]
		stories <- map["stories"]
		events <- map["events"]
		urls <- map["urls"]
	}

}
