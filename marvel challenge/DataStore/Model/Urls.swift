
import Foundation
import ObjectMapper

struct Urls : Mappable {
	var type : String?
	var url : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		type <- map["type"]
		url <- map["url"]
	}

}
