//
//  Coordinator.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation
import UIKit

protocol Coordinator {
    func start()->UIViewController
}
