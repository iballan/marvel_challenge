//
//  AppCoordinator.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation
import UIKit
import Moya

final class AppCoordinator : Coordinator{
    
    private var window : UIWindow?
    
    lazy var marvelWS : MoyaProvider<MarvelWebService> = {
        return MoyaProvider<MarvelWebService>()
    }()
    
    init(window : UIWindow) {
        self.window = window
    }
    var listCoordinator : CharListCoordinator!
    
    @discardableResult
    func start()->UIViewController{
        listCoordinator = CharListCoordinator(marvelWs: marvelWS)
        let mainVC = listCoordinator.start()
        self.window?.rootViewController = mainVC
        self.window?.makeKeyAndVisible()
        return mainVC
    }
}
