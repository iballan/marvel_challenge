//
//  CharListCoordintaor.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation
import UIKit
import Moya

class CharListCoordinator : Coordinator {
    
    var rootViewController: UINavigationController!
    var marvelWs : MoyaProvider<MarvelWebService>
    
    init( marvelWs : MoyaProvider<MarvelWebService>) {
        self.marvelWs = marvelWs
    }
    
    func start() -> UIViewController {
        let listVC = CharListVC()
        let service = CharListServiceImpl(marvelWs: self.marvelWs)
        let viewModel = CharListVMImple(service: service)
        viewModel.coordinatorDelegate = self
        listVC.viewModel = viewModel
        rootViewController = UINavigationController(rootViewController: listVC)
        return rootViewController
    }
}


extension CharListCoordinator : CharListVMCoordinatorDelegate{
    func didTapOnRow(with data: MCharacter) {
        let detailCoordinator = CharDetailsCoordinator(character: data, rootViewController: self.rootViewController, marvelWs: self.marvelWs)
        let detailVc = detailCoordinator.start()
        self.rootViewController.pushViewController(detailVc, animated: true)
    }
}
