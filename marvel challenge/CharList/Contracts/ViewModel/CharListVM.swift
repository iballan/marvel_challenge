//
//  CharListVM.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation
protocol CharListVMCoordinatorDelegate : class{
    func didTapOnRow(with data : MCharacter)
}

protocol CharListVM{
    
    var data : [MCharacter]?{ get }
    var service : CharListService{ get }
    var coordinatorDelegate : CharListVMCoordinatorDelegate?{get set}
    var title : String{ get }
    
    var showData : (()->())?{ get set}
    var showLoading : (()->())?{get set}
    var hideLoading : (()->())?{get set}
    
    func didTapOnChar(of index : Int)
    func heightForRow(at index : Int, of section : Int)->Int
    func numberOfRowsInSection(section : Int)->Int
    func viewDidLoad()
    func getChar(at index : Int)->MCharacter
    func getMoreData()
}
