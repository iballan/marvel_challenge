//
//  CharListService.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation
import Moya

protocol CharListService {
    var marvelWs : MoyaProvider<MarvelWebService> { get }
   
    func getCharList(limit:Int, offset:Int, onCompleted : @escaping([MCharacter], Error?)->())
}

