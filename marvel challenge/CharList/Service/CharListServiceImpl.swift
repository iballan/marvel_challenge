//
//  CharListServiceImpl.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation
import RxSwift
import ObjectMapper
import Moya

class CharListServiceImpl: CharListService {
    
    let disposeBag = DisposeBag()
    var marvelWs: MoyaProvider<MarvelWebService>

    init(marvelWs : MoyaProvider<MarvelWebService>) {
        self.marvelWs = marvelWs
    }
    
    func getCharList(limit: Int, offset: Int, onCompleted: @escaping ([MCharacter], Error?) -> ()) {
        marvelWs.rx.request(MarvelWebService.getChars(limit: limit, offset: offset))
            .subscribe { response in
                let str = String(decoding: response.data, as: UTF8.self)
                let charListRes = CharListResult(JSONString:str)
                if let errorMessage = charListRes?.message {
                    print("Error Message : \(errorMessage)")
                    // If it falls here, we got error
                    onCompleted([], MyError(message: errorMessage))
                    return
                }
                onCompleted(charListRes?.data?.results ?? [], nil)
            } onFailure: { error in
                print("Error \(error)")
                onCompleted([], error)
            }.disposed(by: disposeBag)
    }
    

}
