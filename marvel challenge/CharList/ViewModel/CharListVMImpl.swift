//
//  CharListVMImpl.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation

class CharListVMImple : CharListVM {
    var currentOffset = 0
    var limit = 30
    
    var service: CharListService
    
    weak var coordinatorDelegate: CharListVMCoordinatorDelegate?
    var data: [MCharacter]? = []
    
    var title: String {
        return "Characters"
    }
    
    init(service: CharListService) {
        self.service = service
    }
    
    var showData: (() -> ())?
    
    var showLoading: (() -> ())?
    
    var hideLoading: (() -> ())?
    
    private var isLoading = false{
        didSet{
            (self.isLoading) ? self.showLoading?() : self.hideLoading?()
        }
    }
    
    func didTapOnChar(of index: Int) {
        coordinatorDelegate?.didTapOnRow(with: data![index])
    }
    
    func heightForRow(at index: Int, of section: Int) -> Int {
        200
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return data?.count ?? 0
    }
    
    func viewDidLoad() {
        isLoading = true
        getMoreData()
    }
    
    func getChar(at index: Int) -> MCharacter {
        return data![index]
    }
    
    func getMoreData(){
        self.isLoading = true
        self.service.getCharList(limit: limit, offset: (currentOffset * limit) ) { [weak self] chars, error in
            guard let `self` = self else { return }
            if let error = error {
                print("Error \(error)")
                self.isLoading = false
                return
            }
            if (self.data == nil) {
                self.data = chars
            } else {
                self.data?.append(contentsOf: chars)
            }
            if (!chars.isEmpty) {
                self.currentOffset = self.currentOffset + 1
            }
            self.isLoading = false
            self.showData?()
        }
    }
}
