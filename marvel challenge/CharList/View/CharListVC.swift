//
//  CharListVC.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation
import UIKit
import SnapKit
import SVProgressHUD

class CharListVC : UIViewController {
    private lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        tv.register(UITableViewCell.self, forCellReuseIdentifier: "cellID")
        tv.separatorStyle = .none
        tv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        return tv
    }()
    
    var viewModel : CharListVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Constants.backgroundColor
        navigationController?.navigationBar.prefersLargeTitles = true
        initUI()
        bindVm()
        title = viewModel.title
        
        self.viewModel.viewDidLoad()
    }
    
    func bindVm() {
        self.viewModel.showData = { [weak self] in
            guard let `self` = self else { return }
            self.tableView.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                // Images did not load without this !
                self.tableView.reloadData()
            }
        }
        
        self.viewModel.showLoading = { SVProgressHUD.show() }
        self.viewModel.hideLoading = { SVProgressHUD.dismiss() }
        
    }
    
    func initUI() {
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension CharListVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID")
        let character = self.viewModel.getChar(at: indexPath.row)
        cell?.textLabel?.text = character.name
        cell?.imageView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        if let thumbnail = character.thumbnail {
            cell?.imageView?.setImage(with: "\(thumbnail.path!).\(thumbnail.ext!)")
        } else {
            cell?.imageView?.setImage(with: "")
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.viewModel.didTapOnChar(of: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Load More data logic
        let lastElement = (viewModel.data?.count ?? 0) - 1
        if indexPath.row == lastElement {
            viewModel.getMoreData()
        }
    }
}
