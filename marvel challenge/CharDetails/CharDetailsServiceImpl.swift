//
//  CharDetailsServiceImpl.swift
//  marvel challenge
//
//  Created by MBH on 11/25/21.
//

import Foundation
import RxSwift
import ObjectMapper
import Moya

class CharDetailsServiceImpl: CharDetailsService {
    let disposeBag = DisposeBag()
    var marvelWs: MoyaProvider<MarvelWebService>

    init(marvelWs : MoyaProvider<MarvelWebService>) {
        self.marvelWs = marvelWs
    }
    
    func getCharDetails(by id: Int, onCompleted: @escaping (MCharacter?, Error?) -> ()) {
        marvelWs.rx.request(.getCharDetails(id: id))
//            .filterSuccessfulStatusAndRedirectCodes()
            .subscribe { response in
                let str = String(decoding: response.data, as: UTF8.self)
                let charListRes = CharListResult(JSONString:str)
                if let errorMessage = charListRes?.message {
                    print("Error Message : \(errorMessage)")
                    // If it falls here, we got error
                    onCompleted(nil, MyError(message: errorMessage))
                    return
                }
                onCompleted(charListRes?.data?.results?.first, nil)
            } onFailure: { error in
                print("Error \(error)")
                onCompleted(nil, error)
            }.disposed(by: disposeBag)
    }
    
    func getComics(by id: Int, onCompleted: @escaping ([Comics]?, Error?) -> ()) {
        marvelWs.rx.request(.getComics(id: id))
            .subscribe { response in
                let str = String(decoding: response.data, as: UTF8.self)
                let comListRes = ComicsListResult(JSONString:str)
                if let errorMessage = comListRes?.message {
                    print("Error Message : \(errorMessage)")
                    onCompleted(nil, MyError(message: errorMessage))
                    return
                }
                onCompleted(comListRes?.data?.results ?? [], nil)
            } onFailure: { error in
                print("Error \(error)")
                onCompleted(nil, error)
            }.disposed(by: disposeBag)
    }

}
