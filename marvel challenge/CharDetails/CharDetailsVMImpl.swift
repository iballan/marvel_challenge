//
//  CharDetailsVMImpl.swift
//  marvel challenge
//
//  Created by MBH on 11/25/21.
//

import Foundation
class CharDetailsVMImpl : CharDetailsVM {
    
    var service: CharDetailsService
    
    weak var coordinatorDelegate: CharDetailsVMCoordinatorDelegate?
    var character: MCharacter?
    var comics: [Comics] = []
    var characterId: Int!
    
    var title: String {
        return character?.name ?? ""
    }
    
    init(characterId: Int ,service: CharDetailsService) {
        self.service = service
        self.characterId = characterId
    }
    
    var showData: (() -> ())?
    
    var showLoading: (() -> ())?
    
    var hideLoading: (() -> ())?
    
    private var isLoading = false{
        didSet{
            (self.isLoading) ? self.showLoading?() : self.hideLoading?()
        }
    }
    
    func viewDidLoad() {
        getCharacterDetails()
//        getComics(id: <#T##Int#>)
    }
    
    func getCharacterDetails(){
        self.isLoading = true
        self.service.getCharDetails(by: characterId) { [weak self] chars, error in
            guard let `self` = self else { return }
            if let error = error {
                print("Error \(error)")
                self.isLoading = false
                return
            }
            self.character = chars
            self.getComics()
        }
    }
    func getComics(){
        self.service.getComics(by: characterId) { [weak self] resComics, error in
            guard let `self` = self else { return }
            self.isLoading = false
            if let error = error {
                print("Error \(error)")
                return
            }
            self.comics = resComics?.filter { comic in
                let facDate = comic.dates?.first { $0.type == "focDate" }
                guard let facDate = facDate, let date = facDate.date else {
                    return  true
                }
                return Calendar.current.component(.year, from: date) > 2005
            } ?? []
            self.showData?()
        }
    }
}
