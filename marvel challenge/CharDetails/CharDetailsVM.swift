//
//  CharDetailsVM.swift
//  marvel challenge
//
//  Created by MBH on 11/25/21.
//

import Foundation
protocol CharDetailsVMCoordinatorDelegate : class{
    func didTapOnRow(with data : MCharacter)
}

protocol CharDetailsVM{
    var characterId: Int! {get}
    var comics : [Comics] {get}
    var character : MCharacter?{ get }
    var service : CharDetailsService{ get }
    var coordinatorDelegate : CharDetailsVMCoordinatorDelegate?{get set}
    var title : String{ get }
    
    var showData : (()->())?{ get set}
    var showLoading : (()->())?{get set}
    var hideLoading : (()->())?{get set}
    func viewDidLoad()
}
