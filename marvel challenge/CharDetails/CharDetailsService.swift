//
//  CharDetailsService.swift
//  marvel challenge
//
//  Created by MBH on 11/25/21.
//

import Foundation
import Moya

protocol CharDetailsService {
    var marvelWs : MoyaProvider<MarvelWebService> { get }
   
    func getCharDetails(by id : Int, onCompleted : @escaping(MCharacter?, Error?)->())
    func getComics(by id: Int, onCompleted : @escaping([Comics]?, Error?)->())
}
