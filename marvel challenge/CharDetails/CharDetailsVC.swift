//
//  CharDetailsVC.swift
//  marvel challenge
//
//  Created by MBH on 11/25/21.
//

import Foundation
import UIKit
import SVProgressHUD

class CharDetailsVC : UIViewController {
    private lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        tv.register(UITableViewCell.self, forCellReuseIdentifier: "cellID")
        tv.separatorStyle = .none
        tv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return tv
    }()
    private lazy var header: UIView = {
        let v = UIView()
        return v
    }()
    private lazy var descriptionLabel : UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 5
        lbl.adjustsFontSizeToFitWidth = true
        return lbl
    }()
    private lazy var charImage: UIImageView = {
        let iv = UIImageView()
        iv.layer.cornerRadius = 20
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    private lazy var comicsTitle: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 1
        lbl.font = lbl.font.withSize(24)
        lbl.text = "."
        return lbl
    }()
    
    var characterId: Int!
    var viewModel : CharDetailsVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Constants.backgroundColor
        initUI()
        bindVm()
        title = viewModel.title
        
        self.viewModel.viewDidLoad()
    }
    
    func bindVm() {
        self.viewModel.showData = { [weak self] in
            guard let `self` = self else { return }
            // Show Character Info
            self.tableView.reloadData()
            self.title = self.viewModel.title
            if let thumbnail = self.viewModel.character?.thumbnail {
                self.charImage.setImage(with: "\(thumbnail.path!).\(thumbnail.ext!)")
            }
            self.descriptionLabel.text = self.viewModel.character?.description

            self.comicsTitle.text = !self.viewModel.comics.isEmpty ? "Comics" : ""
            self.tableView.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                // Images did not load without this !
                self.tableView.reloadData()
            }
        }
        
        self.viewModel.showLoading = { SVProgressHUD.show() }
        self.viewModel.hideLoading = { SVProgressHUD.dismiss() }
        
    }
    
    func initUI() {
        view.addSubview(tableView)
        header.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height * 0.45)
        
        header.addSubview(charImage)
        header.addSubview(descriptionLabel)
        header.addSubview(comicsTitle)
        
        tableView.tableHeaderView = header
        charImage.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().offset(10)
            make.trailing.equalToSuperview().offset(-10)
            make.bottom.equalTo(descriptionLabel.snp.top).offset(-10)
        }
        descriptionLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-10)
            make.leading.equalToSuperview().offset(10)
            make.bottom.equalTo(comicsTitle.snp.top).offset(-10)
        }
        comicsTitle.snp.makeConstraints { make in
            make.bottom.trailing.equalToSuperview().offset(-10)
            make.leading.equalToSuperview().offset(10)
        }
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension CharDetailsVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.comics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID")
        let comic = self.viewModel.comics[indexPath.row]
        cell?.textLabel?.text = comic.title
        cell?.textLabel?.numberOfLines = 1
        cell?.imageView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        if let thumbnail = comic.thumbnail {
            cell?.imageView?.setImage(with: "\(thumbnail.path!).\(thumbnail.ext!)")
        } else {
            cell?.imageView?.setImage(with: "")
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("Title: \(self.viewModel.comics[indexPath.row].title!)")
    }
}
