//
//  CharDetailsCoordinator.swift
//  marvel challenge
//
//  Created by MBH on 11/25/21.
//

import Foundation
import UIKit
import Moya

class CharDetailsCoordinator : Coordinator {
    
    let rootViewController: UINavigationController
    let marvelWs : MoyaProvider<MarvelWebService>
    let character: MCharacter
    
    init(character : MCharacter, rootViewController: UINavigationController, marvelWs : MoyaProvider<MarvelWebService>) {
        self.marvelWs = marvelWs
        self.character = character
        self.rootViewController = rootViewController
    }
    
    func start() -> UIViewController {
        let detailVc = CharDetailsVC()
        let service = CharDetailsServiceImpl(marvelWs: self.marvelWs)
        let vm = CharDetailsVMImpl(characterId: character.id!, service: service)
        detailVc.viewModel = vm
        return detailVc
    }
}


extension CharDetailsCoordinator : CharDetailsVMCoordinatorDelegate{
    func didTapOnRow(with data: MCharacter) {
        print("Clicked \(data.name)")
    }
}
