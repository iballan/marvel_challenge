//
//  UIImageView+Extension.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    func setImage(with url: String) {
        guard let _url = URL(string: url) else { return }
        self.setImage(with: _url)
    }
    
    func setImage(with url: URL) {
        self.kf.cancelDownloadTask()
        self.kf.setImage(with: url, placeholder: nil, options: nil, completionHandler: nil)
    }
}
