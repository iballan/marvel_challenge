//
//  String+Extension.swift
//  marvel challenge
//
//  Created by MBH on 11/24/21.
//

import Foundation
import CryptoKit

extension String {
    func md5() -> String {
        return Insecure.MD5.hash(data: self.data(using: .utf8)!).map { String(format: "%02hhx", $0) }.joined()
    }
}
